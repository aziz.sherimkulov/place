﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Place.BL.Models.Place;
using Place.BL.Service.Place;
using Place.Models;
using X.PagedList;

namespace Place.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPlaceService _placeService;

        public HomeController(ILogger<HomeController> logger, IPlaceService placeService)
        {
            _logger = logger;
            _placeService = placeService;
        }

        public IActionResult Index(string value, int? page)        
        {
            ViewBag.Value = value;
            var pageNumber = page ?? 1;
            IEnumerable<PlaceModel> places = _placeService.GetAllPlace(value).ToPagedList(pageNumber, 2);
            return View(places);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
