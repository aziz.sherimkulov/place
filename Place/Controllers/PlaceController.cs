﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Place.BL.Models.Place;
using Place.BL.Models.Review;
using Place.BL.Service;
using Place.BL.Service.Place;
using Place.BL.Service.Review;
using Place.DAL.Entities;
using Place.Models.Image;

namespace Place.Controllers
{
    public class PlaceController : Controller
    {
        private readonly IPlaceService _placeService;
        private readonly IReviewService _reviewService;
        private readonly IImageService _imageService;
        private readonly UserManager<User> _userManager;

        public PlaceController(IPlaceService placeService, UserManager<User> userManager, IReviewService reviewService, IImageService imageService)
        {
            _placeService = placeService;
            _userManager = userManager;
            _reviewService = reviewService;
            _imageService = imageService;
        }
        public async Task<IActionResult> Index(int placeId)
        {
            var place = await _placeService.GetPlaceAsync(placeId);
            return View(place);
        }

        [HttpGet]
        public IActionResult AddNewPlace()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddNewPlace(AddPlaceModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = user.Id;
            if (ModelState.IsValid)
            {
                var placeId = await _placeService.AddNewPlaceAsync(model, userId);

                return RedirectToAction("Index", new { placeId = placeId });
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddReview(AddReviewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = user.Id;

            if (ModelState.IsValid)
            {
                await _reviewService.AddReviewAsync(model, userId);
            }

            return RedirectToAction("Index", new { placeId = model.PlaceId });
        }

        [HttpPost]
        public async Task<IActionResult> AddImage(ImageUploadViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = user.Id;

            if (ModelState.IsValid)
            {
                await _imageService.AddImageAsync(model, userId);
            }

            return RedirectToAction("Index", new { placeId = model.PlaceId });
        }
    }
}