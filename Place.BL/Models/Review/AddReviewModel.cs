﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Place.BL.Models.Review
{
    public class AddReviewModel
    {
        [Required]
        public string Comment { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "{0} оценка от  {1} до {2}")]
        public int Rating { get; set; }

        [Required]

        public int PlaceId { get; set; }


        public SelectList RatingSelect { get; set; }       


    }
}
