﻿using System;

namespace Place.BL.Models.Review
{
    public class ReviewModel
    {
        public DateTime PostDateTime { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public double Rating { get; set; }
    }
}