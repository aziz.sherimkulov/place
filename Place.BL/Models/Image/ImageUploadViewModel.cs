﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Place.Models.Image
{
    public class ImageUploadViewModel
    {
        [Required]
        [Display(Name = "Загрузите изображение")]
        public IFormFile Image { get; set; }

        [Required]
        public int PlaceId { get; set; }
    }
}
