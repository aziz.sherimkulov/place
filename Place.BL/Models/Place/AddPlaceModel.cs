﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Place.BL.Models.Place
{
    public class AddPlaceModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Изображение")]
        public IFormFile MainImage { get; set; }
    }
}
