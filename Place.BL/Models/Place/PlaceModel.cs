﻿namespace Place.BL.Models.Place
{
    public class PlaceModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string MainImageFilePath { get; set; }

        public int ReviewsCount { get; set; }

        public int PicturesCount { get; set; }
    }
}
