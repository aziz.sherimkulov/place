﻿using System.Collections.Generic;
using Place.BL.Models.Review;

namespace Place.BL.Models.Place
{
    public class PlaceInfoModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MainImageFilePath { get; set; }
        public double AverageRating { get; set; }
        public List<string> ImagesFilePath { get; set; }
        public List<ReviewModel> Reviews { get; set; }
    }
}
