﻿using System;
using AutoMapper;
using Place.BL.Models.Review;

namespace Place.BL.Mapper.Review
{
    public class ReviewModelToDomainProfile : Profile
    {
        public ReviewModelToDomainProfile()
        {
            AddReviewModelToReviewMapping();
        }

        private void AddReviewModelToReviewMapping()
        {
            CreateMap<AddReviewModel, DAL.Entities.Review>()
                .ForMember(target => target.PostDateTime,
                    src => src.MapFrom(r => DateTime.Now))
                ;
        }
    }
}