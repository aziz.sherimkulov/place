﻿using AutoMapper;
using Place.BL.Models.Review;

namespace Place.BL.Mapper.Review
{
    public class DomainToReviewModelProfile : Profile
    {
        public DomainToReviewModelProfile()
        {
            ReviewToReviewModelMapping();
        }

        private void ReviewToReviewModelMapping()
        {
            CreateMap<DAL.Entities.Review, ReviewModel>()
                .ForMember(target => target.UserName, 
                    src => src.MapFrom(r => r.User.Email))
                ;
        }
    }
}