﻿using AutoMapper;
using Place.BL.Mapper.Review;

namespace Place.BL.Mapper
{
    public class MappingConfiguration
    {
        public MapperConfiguration RefisterMappings()
        {
            return new MapperConfiguration(map =>
            {
                map.AddProfile(new ReviewModelToDomainProfile());
                map.AddProfile(new DomainToReviewModelProfile());
            });
        }
    }
}
