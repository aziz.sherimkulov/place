﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Place.BL.Models.Review;
using Place.DAL.UnitOfWork;

namespace Place.BL.Service.Review
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public ReviewService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public SelectList GetSelectListRation(int number = 5, int? selectedNumber = null)
        {
            return new SelectList(Enumerable.Range(1, number).Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString()
            }), "Value", "Text", selectedNumber);
        }

        public async Task AddReviewAsync(AddReviewModel model, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var review = _mapper.Map<DAL.Entities.Review>(model);
                review.UserId = userId;

                await unitOfWork.ReviewRepository.CreateAsync(review);
                await unitOfWork.CompleteAsync();
            }
        }
    }
}