﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Place.BL.Models.Review;

namespace Place.BL.Service.Review
{
    public interface IReviewService
    {
        Task AddReviewAsync(AddReviewModel model, int userId);
        SelectList GetSelectListRation(int number, int? selectedNumber = null);
    }
}