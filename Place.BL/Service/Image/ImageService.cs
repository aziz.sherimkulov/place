﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Place.DAL.Entities;
using Place.DAL.UnitOfWork;
using Place.Models.Image;

namespace Place.BL.Service
{
    public class ImageService : IImageService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ImageService(IUnitOfWorkFactory unitOfWorkFactory, IWebHostEnvironment webHostEnvironment)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<string> UploadImage(IFormFile file)
        {
            string uniqueFileName = null;
            if (file != null)
            {
                string uploadsFolder = _webHostEnvironment.WebRootPath;
                uniqueFileName = Path.Combine("images", Guid.NewGuid().ToString() + "_" + file.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                await file.CopyToAsync(new FileStream(filePath, FileMode.Create));
            }

            return uniqueFileName;
        }
        public async Task AddImageAsync(ImageUploadViewModel model, int userId)
        {
            string uniqueFileName = await UploadImage(model.Image);

            var image = new Image
            {
                PlaceId = model.PlaceId,
                ImagePath = uniqueFileName,
                UserId = userId
            };

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                await unitOfWork.ImageRepository.CreateAsync(image);
                await unitOfWork.CompleteAsync();
            }
        }
    }
}