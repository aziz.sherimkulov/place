﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Place.Models.Image;

namespace Place.BL.Service
{
    public interface IImageService
    {
        Task<string> UploadImage(IFormFile file);
        Task AddImageAsync(ImageUploadViewModel model, int userId);
    }
}