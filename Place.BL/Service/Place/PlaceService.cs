﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Place.BL.Models.Place;
using Place.BL.Models.Review;
using Place.DAL.UnitOfWork;

namespace Place.BL.Service.Place
{
    public class PlaceService : IPlaceService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IImageService _imageService;
        private readonly IMapper _mapper;

        public PlaceService(IUnitOfWorkFactory unitOfWorkFactory, IImageService imageService, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _imageService = imageService;
            _mapper = mapper;
        }

        public List<PlaceModel> GetAllPlace(string value)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var places = unitOfWork.PlaceRepository.GetAllIncludeReviewsAndImages(value);

                var placesModels = places
                    .Select(x =>
                        new PlaceModel()
                        {
                            Id = x.Id,
                            MainImageFilePath = x.MainImagePath,
                            PicturesCount = x.Images.Count() + 1,
                            ReviewsCount = x.Reviews.Count() + 1,
                            Title = x.Title
                        }).ToList();


                return placesModels;
            }
        }

        public async Task<int> AddNewPlaceAsync(AddPlaceModel model, int userId)
        {
            string fileName = await _imageService.UploadImage(model.MainImage);

            var place = new DAL.Entities.Place()
            {
                Title = model.Title,
                Description = model.Description,
                MainImagePath = fileName,
                UserId = userId
            };

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                await unitOfWork.PlaceRepository.CreateAsync(place);
                await unitOfWork.CompleteAsync();
            }

            return place.Id;
        }

        public async Task<PlaceInfoModel> GetPlaceAsync(int placeId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var place = await unitOfWork.PlaceRepository.GetByIdIncludeReviewsAndImagesAsync(placeId);

                var placeModel = new PlaceInfoModel
                {
                    Id = place.Id,
                    Description = place.Description,
                    Title = place.Title,
                    MainImageFilePath = place.MainImagePath,
                    ImagesFilePath = place.Images.Select(x => x.ImagePath).ToList(),
                    AverageRating = Math.Round(place.Reviews.Average(r => r?.Rating) ?? 0, 1),
                    Reviews = place.Reviews.Select(x => new ReviewModel
                    {
                        UserName = x.User.Email,
                        Rating = x.Rating,
                        Comment = x.Comment,
                        PostDateTime = x.PostDateTime
                    }).ToList()
                };


                return placeModel;
            }
        }
    }
}