﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Place.BL.Models.Place;

namespace Place.BL.Service.Place
{
    public interface IPlaceService
    {
        List<PlaceModel> GetAllPlace(string value);
        Task<int> AddNewPlaceAsync(AddPlaceModel model, int userId);
        Task<PlaceInfoModel> GetPlaceAsync(int placeId);
    }
}