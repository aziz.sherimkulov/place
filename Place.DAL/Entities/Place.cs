﻿using System.Collections.Generic;

namespace Place.DAL.Entities
{
    public class Place : IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MainImagePath { get; set; }

        public IEnumerable<Image> Images { get; set; }
        public IEnumerable<Review> Reviews { get; set; }

    }
}