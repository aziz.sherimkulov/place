﻿namespace Place.DAL.Entities
{
    public class Image : IEntity
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }

        public Place Place { get; set; }
        public int PlaceId { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }      
    }
}