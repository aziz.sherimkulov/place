﻿using Microsoft.AspNetCore.Identity;

namespace Place.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        
    }
}