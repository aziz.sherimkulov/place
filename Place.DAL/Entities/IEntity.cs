﻿namespace Place.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}