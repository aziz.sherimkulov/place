﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Place.DAL.Entities;

namespace Place.DAL.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        Task<int> CreateAsync(T entity);
        Task<List<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
    }
}