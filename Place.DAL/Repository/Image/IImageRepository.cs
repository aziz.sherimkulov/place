﻿using Place.DAL.Entities;

namespace Place.DAL.Repository
{
    public interface IImageRepository : IRepository<Image>
    {
    }
}