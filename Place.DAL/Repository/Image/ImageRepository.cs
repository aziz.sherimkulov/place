﻿using Place.DAL.Context;
using Place.DAL.Entities;

namespace Place.DAL.Repository
{
    public class ImageRepository : Repository<Image>, IImageRepository
    {
        public ImageRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}