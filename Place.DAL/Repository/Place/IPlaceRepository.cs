﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Place.DAL.Repository.Place
{
    public interface IPlaceRepository : IRepository<Entities.Place>
    {
        IEnumerable<Entities.Place> GetAllIncludeReviewsAndImages(string value);
        Task<Entities.Place> GetByIdIncludeReviewsAndImagesAsync(int Id);
    }
}