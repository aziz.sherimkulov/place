﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Place.DAL.Context;

namespace Place.DAL.Repository.Place
{
    public class PlaceRepository : Repository<Entities.Place>, IPlaceRepository
    {
        public PlaceRepository(ApplicationDbContext context) : base(context)
        {
        }
        public IEnumerable<Entities.Place> GetAllIncludeReviewsAndImages(string value)
        {
            return DbSet
                .Include(x => x.Images)
                .Include(x => x.Reviews);
        }
        public async Task<Entities.Place> GetByIdIncludeReviewsAndImagesAsync(int Id)
        {
            return await DbSet
                .Include(x => x.Images)
                .Include(x => x.Reviews)
                .ThenInclude(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == Id);
        }
    }
}