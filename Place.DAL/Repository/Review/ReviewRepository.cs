﻿using Place.DAL.Context;

namespace Place.DAL.Repository.Review
{
    public class ReviewRepository : Repository<Entities.Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}