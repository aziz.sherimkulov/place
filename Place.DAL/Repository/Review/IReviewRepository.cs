﻿namespace Place.DAL.Repository.Review
{
    public interface IReviewRepository : IRepository<Entities.Review>
    {
    }
}