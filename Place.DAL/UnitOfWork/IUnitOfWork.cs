﻿using System;
using System.Threading.Tasks;
using Place.DAL.Repository;
using Place.DAL.Repository.Place;
using Place.DAL.Repository.Review;

namespace Place.DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IImageRepository ImageRepository { get; }
        IPlaceRepository PlaceRepository { get; }
        IReviewRepository ReviewRepository { get; }
        Task<int> CompleteAsync();
    }
}