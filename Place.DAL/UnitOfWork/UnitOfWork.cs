﻿using System.Threading.Tasks;
using Place.DAL.Context;
using Place.DAL.Repository;
using Place.DAL.Repository.Place;
using Place.DAL.Repository.Review;

namespace Place.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly ApplicationDbContext _context;


        public IImageRepository ImageRepository { get; }
        public IPlaceRepository PlaceRepository { get; }
        public IReviewRepository ReviewRepository { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            ImageRepository = new ImageRepository(context);
            PlaceRepository = new PlaceRepository(context);
            ReviewRepository = new ReviewRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}