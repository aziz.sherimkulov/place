﻿namespace Place.DAL.Context
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateDbContext();
    }
}