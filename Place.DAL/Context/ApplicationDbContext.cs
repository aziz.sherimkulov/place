﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Place.DAL.Entities;

namespace Place.DAL.Context
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        public ApplicationDbContext
            (DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Entities.Place> Places { get; set; }
            
        public DbSet<Image> Images { get; set; }

        public DbSet<Review> Reviews { get; set; }

    }
}