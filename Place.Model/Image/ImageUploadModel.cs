﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Place.Model.Image
{
    public class ImageUploadModel
    {
        [Required]
        [Display(Name = "Загрузите изображение")]
        public IFormFile Image { get; set; }

        [Required]
        public int PlaceId { get; set; }
    }
}
